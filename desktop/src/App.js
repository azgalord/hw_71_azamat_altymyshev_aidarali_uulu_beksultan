import React, { Component } from 'react';
import {Switch, Route} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import Dishes from "./containers/Dishes/Dishes";
import OrdersPage from "./containers/OrdersPage/OrdersPage";

import './App.css';

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={Dishes}/>
          <Route path="/orders" exact component={OrdersPage}/>
        </Switch>
      </Layout>
    );
  }
}

export default App;

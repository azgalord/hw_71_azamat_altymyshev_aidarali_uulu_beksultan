import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://burger-project-azamat60.firebaseio.com/'
});

export default instance;
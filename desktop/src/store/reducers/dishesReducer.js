import {INIT_DISHES} from "../actions/ActionTypes";

const initialState = {
  dishes: null,
};

const dishesReducer = (state = initialState, action) => {
  switch (action.type) {
    case INIT_DISHES:
      return {
        ...state,
        dishes: action.dishes
      };
    default:
      return state;
  }
};

export default dishesReducer;
import {INIT_ORDERS} from "../actions/ActionTypes";

const initialState = {
  orders: null
};

const ordersReducer = (state = initialState, action) => {
  switch (action.type) {
    case INIT_ORDERS:
      return {
        ...state,
        orders: action.orders.data,
      };
    default:
      return state;
  }
};

export default ordersReducer;
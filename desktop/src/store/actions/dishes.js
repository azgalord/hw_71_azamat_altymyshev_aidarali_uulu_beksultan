import {DISHES_FAILURE, DISHES_REQUEST, DISHES_SUCCESS, INIT_DISHES} from "./ActionTypes";
import axios from '../../axios-dishes';

export const initDishes = (dishes) => ({type: INIT_DISHES, dishes});

export const dishesRequest = () => ({type: DISHES_REQUEST});
export const dishesSuccess = () => ({type: DISHES_SUCCESS});
export const dishesFailure = error => ({type: DISHES_FAILURE, error});

export const postDishes = (dish) => {
  return dispatch => {
    dispatch(dishesRequest());

    axios.post('/dishes.json', dish).then(
      response => dispatch(dishesSuccess()),
      error => dispatch(dishesFailure(error)),
    )
  }
};

export const fetchDishes = () => {
  return dispatch => {
    dispatch(dishesRequest());

    axios.get('/dishes.json').then(
      response => dispatch(initDishes(response.data)),
      error => dispatch(dishesFailure(error))
    );
  }
};
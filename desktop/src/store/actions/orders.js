import axios from "../../axios-dishes";
import {INIT_ORDERS} from "./ActionTypes";

export const initOrders = (orders) => ({type: INIT_ORDERS, orders});
export const completeOrder = (order) => {
  return dispatch => {
    axios.delete('/dishOrders/' + order + '.json');
    dispatch(fetchOrders());
  }
};

export const fetchOrders = () => {
  return dispatch => {
    axios.get('/dishOrders.json').then(
      response => dispatch(initOrders(response))
    )
  }
};
import React from 'react';
import {Link} from "react-router-dom";

import './Header.css';

const Header = () => {
  return (
    <header className="Header">
      <nav className="HeaderNav">
        <Link to="/">Dishes</Link>
        <Link to="/orders">Orders</Link>
      </nav>
    </header>
  );
};

export default Header;
import React from 'react';
import './Form.css';

const Form = props => {
  return (
    <form>
      <input
        value={props.nameValue} onChange={props.change}
        type="text" placeholder="Name of dish"
        name="name"
      />
      <input
        value={props.priceValue} onChange={props.change}
        type="text" placeholder="Price of dish"
        name="price"
      />
      <input
        value={props.imageValue} onChange={props.change}
        type="text" placeholder="Url of image of dish"
        name="image"
      />
      <button onClick={props.sendDish} className="Add">{props.button}</button>
    </form>
  );
};

export default Form;

import React from 'react';
import Form from "../../Form/Form";

import './Modal.css';

const Modal = props => {
  return (
    <div style={{display: props.modalVisible ? 'block' : 'none'}} className="ModalBg">
      <div className="Modal">
        <h2>{props.title}</h2>
        <Form
          nameValue={props.nameValue}
          priceValue={props.priceValue}
          imageValue={props.imageValue}
          change={props.change}
          sendDish={props.sendDish}
          button={props.buttonValue}
        />
        <button onClick={props.modalClose} className="CloseBtn">X</button>
      </div>
    </div>
  );
};

export default Modal;

import React from 'react';

import './AddButton.css';

const AddButton = props => {
  return (
    <button onClick={props.click} className="AddButton">Add new dish</button>
  );
};

export default AddButton;
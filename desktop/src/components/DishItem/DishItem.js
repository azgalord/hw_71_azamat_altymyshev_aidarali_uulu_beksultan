import React from 'react';

import './DishItem.css';

const DishItem = props => {
  return (
    <div className="DishItem">
      <img src={props.image} alt=""/>
      <h5>{props.name}</h5>
      <span>{props.price} KGS</span>
      <button onClick={props.edit} className="Edit">Edit</button>
      <button onClick={props.delete} className="Delete">Delete</button>
    </div>
  );
};

export default DishItem;

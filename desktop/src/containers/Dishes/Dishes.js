import React, {Component} from 'react';
import AddButton from "../../components/DishesComponents/AddButton/AddButton";
import Modal from "../../components/UI/Modal/Modal";
import {connect} from "react-redux";
import {fetchDishes, postDishes} from "../../store/actions/dishes";
import DishItem from "../../components/DishItem/DishItem";

import axios from '../../axios-dishes';

import './Dishes.css';

class Dishes extends Component {
  state = {
    modal: false,
    edit: false,
    dish: {
      name: '',
      price: '',
      image: ''
    },
    editIndex: null
  };

  componentDidMount() {
    this.props.fetchDishes();
  }

  modalToggle = () => {
    this.setState({modal: !this.state.modal})
  };

  changeHandler = (event) => {
    this.setState({
      dish: {
        ...this.state.dish,
        [event.target.name]: event.target.value
      }
    });
  };

  editClose = () => {
    this.setState({edit: false, dish: {
        ...this.state.dish,
        name: '',
        price: '',
        image: '',
    }})
  };

  sendDish = (event) => {
    event.preventDefault();

    const dish = this.state.dish;
    this.props.postDishes(dish);

    this.setState({
      dish: {
        ...this.state.dish,
        name: '',
        price: '',
        image: '',
      }
    });
    this.modalToggle();
  };

  sendEditedDish = (event) => {
    event.preventDefault();

    const keys = Object.keys(this.props.dishes);
    const thisKey = keys[this.state.editIndex];
    const dishes = {
      ...this.props.dishes,
      [thisKey] : {
        ...this.props.dishes[thisKey],
        image: this.state.dish.image,
        name: this.state.dish.name,
        price: this.state.dish.price,
      }
    };

    axios.put('/dishes.json', dishes).then(() => {
        this.editClose();
    });
    this.setState({dishes: {name: '', price: '', image: ''}});
  };

  editButtonClicked = (name, image, price, index) => {
    this.setState({
      ...this.state,
      dish: {
        ...this.state.dish,
        name: name,
        image: image,
        price: price,
      },
      edit: true,
      editIndex: index,
    });
  };

  deleteDish = (index) => {
    const keys = Object.keys(this.props.dishes);

    axios.delete('/dishes/' + keys[index] + '.json');
  };

  render() {
    if (!this.props.dishes){
      return (<div>Loading...</div>)
    }
    return (
      <div className="Dishes">
        <AddButton
          click={this.modalToggle}
        />
        <div className="DishesItems">
          {Object.values(this.props.dishes).map((dish, index) => (
            <DishItem
              name={dish.name}
              image={dish.image}
              price={dish.price}
              key={index}
              edit={() => this.editButtonClicked(dish.name, dish.image, dish.price, index)}
              delete={() => this.deleteDish(index)}
            />
          ))}
        </div>
        <Modal
          title="Add new dishes"
          buttonValue="Add"
          modalVisible={this.state.modal}
          modalClose={this.modalToggle}
          nameValue={this.state.dish.name}
          priceValue={this.state.dish.price}
          imageValue={this.state.dish.image}
          change={(event) => this.changeHandler(event)}
          sendDish={(event) => this.sendDish(event)}
        />
        <Modal
          title="Edit dish"
          buttonValue="Edit"
          modalVisible={this.state.edit}
          modalClose={this.editClose}
          nameValue={this.state.dish.name}
          priceValue={this.state.dish.price}
          imageValue={this.state.dish.image}
          change={(event) => this.changeHandler(event)}
          sendDish={(event) => this.sendEditedDish(event)}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    dishes: state.dishes.dishes,
  }
};

const mapDispatchToProps = dispatch => ({
    postDishes: dish => dispatch(postDishes(dish)),
    fetchDishes: () => dispatch(fetchDishes()),
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(Dishes);

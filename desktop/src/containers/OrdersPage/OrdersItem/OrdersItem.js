import React from 'react';

import './OrdersItem.css';

const OrdersItem = props => {
  return (
    <div className="OrdersItem">
      <div className="OrdersItemContainer">
        {props.children}
      </div>
      <div className="OrdersMainInfo">
        <p>Delivery: {props.delivery} KGS</p>
        <p>Total: {props.total} KGS</p>
        <button onClick={props.complete}>Complete Order</button>
      </div>
    </div>
  );
};

export default OrdersItem;
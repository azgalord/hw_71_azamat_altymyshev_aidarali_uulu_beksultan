import React from 'react';

import './OrderRow.css';

const OrderRow = props => (
  <p className="OrderRow">{props.mount} x {props.name}</p>
);

export default OrderRow;
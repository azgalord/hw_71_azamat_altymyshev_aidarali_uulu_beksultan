import React, {Component} from 'react';
import {connect} from "react-redux";
import {completeOrder, fetchOrders} from "../../store/actions/orders";
import {fetchDishes} from "../../store/actions/dishes";

import './OrdersPage.css';
import OrdersItem from "./OrdersItem/OrdersItem";
import OrderRow from "./OrderRow/OrderRow";

const DELIVERY = 150;

class OrdersPage extends Component {

  componentDidMount() {
    this.props.fetchOrders();
    this.props.fetchDishes();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log('updated');
  }

  render() {
    if (!this.props.orders || !this.props.dishes) {
      return (
        <div>Loading...</div>
      );
    }

    return (
      <div className="OrdersPage">
        <h2>Orders</h2>
        <div className="OrdersItems">
          {Object.values(this.props.orders).map((order, index) => {
            const keys = Object.keys(this.props.orders);
            const keysOfOrder = Object.keys(order);
            const dishes = this.props.dishes;

            const mounts = Object.values(order);
            const prices = keysOfOrder.map(orderValue => dishes[orderValue].price);
            let totalPrice = DELIVERY;

            for (let key in mounts) {
              totalPrice += mounts[key] * prices[key];
            }


            return (
              <OrdersItem
                key={index}
                complete={() => this.props.completeOrder(keys[index])}
                delivery={DELIVERY}
                total={totalPrice}
              >
                {Object.values(order).map((itemValue, index) => (
                  <OrderRow
                    key={index}
                    mount={itemValue}
                    name={this.props.dishes[keysOfOrder[index]].name}
                  />
                ))}
              </OrdersItem>
            )})}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  orders: state.orders.orders,
  dishes: state.dishes.dishes,
});

const mapDispatchToProps = dispatch => ({
  fetchOrders: () => dispatch(fetchOrders()),
  fetchDishes: () => dispatch(fetchDishes()),
  completeOrder: (order) => dispatch(completeOrder(order)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrdersPage);
import {
  CANCEL_PRESSED,
  DELETE_ORDER,
  DISHES_FAILURE,
  DISHES_REQUEST,
  DISHES_SUCCESS,
  ORDER_DISH,
  TOGGLE_MODAL
} from "./actionTypes";
import axios from '../axios-mobile';

export const dishesRequest = () => ({type: DISHES_REQUEST});
export const dishesSuccess = (response) => ({type: DISHES_SUCCESS, response});
export const dishesFailure = (error) => ({type: DISHES_FAILURE, error});

export const toggleModal = () => ({type: TOGGLE_MODAL});
export const orderDish = (order) => ({type: ORDER_DISH, order});
export const deleteOrder = (order) => ({type: DELETE_ORDER, order});

export const cancelPressed = () => ({type: CANCEL_PRESSED});

export const fetchRequest = () => {
  return dispatch => {
    dispatch(dishesRequest());

    axios.get('/dishes.json').then(
      response => dispatch(dishesSuccess(response)),
      error => dispatch(dishesFailure(error)),
    )
  }
};

export const sendOrder = (order) => {
  return dispatch => {
    dispatch(dishesRequest());

    axios.post('/dishOrders.json', order).then(
      response => dispatch(cancelPressed()),
      error => dispatch(dishesFailure(error)),
    );
  }
};

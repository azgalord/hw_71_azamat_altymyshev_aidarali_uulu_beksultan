import {CANCEL_PRESSED, DELETE_ORDER, DISHES_SUCCESS, ORDER_DISH, TOGGLE_MODAL} from "./actionTypes";

const initialState = {
  dishes: {},
  orders: {},
  ordersForSend: {},
  delivery: 150,
  total: 150,
  modal: false,
};

const mobileReducer = (state = initialState, action) => {
  switch (action.type) {
    case DISHES_SUCCESS:
      return {
        ...state,
        dishes: action.response.data,
      };
    case TOGGLE_MODAL:
      return {
        ...state,
        modal: !state.modal,
      };
    case ORDER_DISH:
      const orders = {...state.orders};
      const ordersForSend = {...state.ordersForSend};

      if (ordersForSend[action.order.id]) {
        ordersForSend[action.order.id] = ordersForSend[action.order.id] + 1;
        orders[action.order.id] = action.order;
        orders[action.order.id].mount = orders[action.order.id].mount + 1;
      } else {
        ordersForSend[action.order.id] = 1;
        orders[action.order.id] = action.order;
        orders[action.order.id].mount = 1;
      }

      const allOrders = Object.values(orders);
      const allMounts = Object.values(ordersForSend);
      let total = 0;

      for (let i = 0; i < allOrders.length; i++) {
        total += allMounts[i] * allOrders[i].price;
      }
      total += state.delivery;

      return {
        ...state,
        orders: orders,
        total: total,
        ordersForSend: ordersForSend
      };
    case DELETE_ORDER:
      const ordersDeleted = {...state.orders};
      const ordersForSendDeleted = {...state.ordersForSend};
      delete ordersDeleted[action.order];
      delete ordersForSendDeleted[action.order];

      const allOrdersChanged = Object.values(ordersDeleted);
      const allMountsChanged = Object.values(ordersForSendDeleted);
      let newTotal = 0;

      for (let i = 0; i < allOrdersChanged.length; i++) {
        newTotal += allMountsChanged[i] * allOrdersChanged[i].price;
      }
      newTotal += state.delivery;

      return {
        ...state,
        orders: ordersDeleted,
        ordersForSend: ordersForSendDeleted,
        total: newTotal,
      };
    case CANCEL_PRESSED:
      return {
        ...state,
        orders: {},
        ordersForSend: {},
        total: 150,
        modal: false,
      };
    default:
      return state;
  }
};

export default mobileReducer;

import React, {Component} from 'react';
import {StyleSheet, FlatList, View} from 'react-native';
import Dish from "../Dish/Dish";
import {fetchRequest, orderDish} from "../../store/mobile";
import {connect} from "react-redux";

const styles = StyleSheet.create({
  flatList: {
    flex: 5,
  }
});

class Dishes extends Component {
  componentDidMount() {
    this.props.fetchRequest();
  }

  getData = (data) => {
    const dataArray = Object.values(data);
    const keysArray = Object.keys(data);
    for (let key in dataArray) {
      dataArray[key].id = keysArray[key];
    }
    // console.log(dataArray);
    return dataArray;
  };

  _keyExtractor = (item, index) => item.id;

  render(){
    return (
      <View style={styles.flatList}>
        <FlatList
          data={this.getData(this.props.dishes)}
          keyExtractor={this._keyExtractor}
          renderItem={({item}) => (
            <Dish
              press={() => this.props.orderDish(item)}
              image={item.image}
              name={item.name}
              price={item.price}
            />
          )}
        />
      </View>
    )
  }
}


const mapStateToProps = state => ({
  dishes: state.dishes
});

const mapDispatchToProps = dispatch => ({
  fetchRequest: () => dispatch(fetchRequest()),
  orderDish: (dish) => dispatch(orderDish(dish))
});

export default connect(mapStateToProps, mapDispatchToProps)(Dishes);

import React from 'react';
import {StyleSheet, TouchableOpacity, View, Image, Text} from 'react-native';

const styles = StyleSheet.create({
  dishContainer: {
    flex: 1,
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});

const Dish = props => (
  <TouchableOpacity onPress={props.press}>
    <View style={styles.dishContainer}>
      <Image source={{uri: props.image}} style={{width: 40, height: 40}}/>
      <Text>Name: {props.name}</Text>
      <Text>Price: {props.price}</Text>
    </View>
  </TouchableOpacity>
);

export default Dish;

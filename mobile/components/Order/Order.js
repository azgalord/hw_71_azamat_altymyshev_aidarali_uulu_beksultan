import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, View, Text} from 'react-native';
import {connect} from "react-redux";
import {toggleModal} from "../../store/mobile";

const styles = StyleSheet.create({
  checkout: {
    flex: 1,
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  checkoutButton: {
    borderColor: '#000',
    borderWidth: 1,
    borderStyle: 'solid',
    paddingHorizontal: 15,
  }
});

class Order extends Component {
  render() {
    return (
      <View style={styles.checkout}>
        <Text>Order total: {this.props.total} KGS</Text>
        <TouchableOpacity onPress={this.props.toggleModal} style={styles.checkoutButton}>
          <Text>Checkout</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  total: state.total,
});

const mapDispatchToProps = dispatch => ({
  toggleModal: () => dispatch(toggleModal()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Order);

import React, {Component} from 'react';
import {StyleSheet, Modal, TouchableOpacity, View, Text} from 'react-native';
import {connect} from "react-redux";
import {deleteOrder, cancelPressed, sendOrder} from "../../store/mobile";

const styles = StyleSheet.create({
  modal: {
    backgroundColor: '#ffffff',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
    paddingHorizontal: 30,
    paddingTop: 70,
  }
});

class ModalComponent extends Component {

  render() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.modal}
      >
        <View style={styles.modal}>
          <View style={{height: 20, marginBottom: 40}}>
            <Text>Your order</Text>
          </View>
          <View style={{flexDirection: 'column', marginBottom: 50}}>
            {Object.values(this.props.orders).map(order => (
              <View style={{height: 40, flexDirection: 'row', justifyContent: 'spaceBetween'}} key={order.id}>
                <Text>{order.name} x </Text>
                <Text style={{marginRight: 30}}>{order.mount}</Text>
                <Text style={{marginRight: 30}}>{order.mount * order.price}</Text>
                <TouchableOpacity onPress={() => this.props.deleteOrder(order.id)}>
                  <View style={{backgroundColor: '#000', paddingHorizontal: 5}}>
                    <Text style={{color: '#fff', textAlign: 'center'}}>Delete</Text>
                  </View>
                </TouchableOpacity>
              </View>
              ))}
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text>Delivery</Text>
            <Text>{this.props.delivery} KGS</Text>
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom: 60}}>
            <Text>Total</Text>
            <Text>{this.props.total} KGS</Text>
          </View>
          <View>
            <TouchableOpacity onPress={this.props.cancelPressed} style={{marginBottom: 5, height: 50}}>
              <View style={{backgroundColor: '#000', paddingHorizontal: 5, flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{color: '#fff', textAlign: 'center'}}>Cancel</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity onPress={() => this.props.sendOrder(this.props.ordersForSend)} style={{marginBottom: 5, height: 50}}>
              <View style={{backgroundColor: '#000', paddingHorizontal: 5, flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{color: '#fff', textAlign: 'center'}}>Order</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    )
  }
}

const mapStateToProps = state => ({
  modal: state.modal,
  orders: state.orders,
  total: state.total,
  delivery: state.delivery,
  ordersForSend: state.ordersForSend
});

const mapDispatchToProps = dispatch => ({
  deleteOrder: (order) => dispatch(deleteOrder(order)),
  cancelPressed: () => dispatch(cancelPressed()),
  sendOrder: (order) => dispatch(sendOrder(order))
});

export default connect(mapStateToProps, mapDispatchToProps)(ModalComponent);

import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import {createStore, applyMiddleware, compose} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {Provider} from 'react-redux';
import mobileReducer from './store/mobileReducer';

import Dishes from './components/Dishes/Dishes';
import Order from "./components/Order/Order";
import ModalComponent from "./components/ModalComponent/Modal";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(mobileReducer, composeEnhancers(applyMiddleware(thunkMiddleware)));

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'center',
    paddingHorizontal: 20,
    paddingTop: 30
  },
  title: {
    alignItems: 'center',
    flex: 1,
    height: 50,
  },
});

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <View style={styles.title}>
            <Text>Turtle Pizza</Text>
          </View>
          <Dishes/>
          <Order/>
          <ModalComponent/>
        </View>
      </Provider>
    );
  }
}

export default App;
